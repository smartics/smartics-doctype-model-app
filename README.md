# projectdoc App Doctype Models Model


## Overview

This project specifies a model to create a free [doctype add-on](https://www.smartics.eu/confluence/x/nQHJAw)
for the [projectdoc Toolbox](https://www.smartics.eu/confluence/x/1YEp)
for [Confluence](https://www.atlassian.com/software/confluence).

The add-on provides base blueprints for app documentation, including

  * App
  * App Component
  * App Extension
  * App Macro
  * App Parameter
  * App Platform
  * App Service
  * App Tool
  * Doctype
  * Document Property
  * Document Section
  * Page Blueprint
  * Space Blueprint
  * Space Property

## Fork me!
Feel free to fork this project to adjust the model according to your project
requirements.

The Doctypes Model and it's derived artifacts are licensed under
[Apache License Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)

## Documentation

For more information please visit

  * the [add-on's homepage](https://www.smartics.eu/confluence/x/gIH-AQ)
  * [projectdoc Toolbox Online Manual](https://www.smartics.eu/confluence/x/EAFk)
  * [Doctype Maven Plugin](https://www.smartics.eu/confluence/x/zgDqAw)
