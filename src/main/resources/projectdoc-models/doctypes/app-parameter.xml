<?xml version='1.0'?>
<!--

    Copyright 2014-2025 Kronseder & Reiner GmbH, smartics

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<doctype
  xmlns="http://smartics.de/xsd/projectdoc/doctype/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  id="app-parameter"
  base-template="standard"
  provide-type="standard-type"
  category="configuration">
  <resource-bundle>
    <l10n>
      <name>Parameter</name>
      <description>
        Document a parameter for your app.
      </description>
      <about>
        A parameter is a configuration option. Parameter exist on different
        layers. Wizard and macro parameters are configuration options. Use
        parameter types to organize them!
      </about>
    </l10n>
    <l10n locale="de">
      <name plural="Parameter">Parameter</name>
      <description>
       Dokumentieren Sie einen Parameter Ihrer App.
      </description>
      <about>
        Parameter erlauben die Konfiguration der App auf verschiedenen Ebenen.
        Bereichseigenschaften, Wizard- und Makroparameter sind solche
        Konfigurationsoptionen. Verwenden Sie Parametertypen um die Parameter
        zu organisieren.
      </about>
      <type plural="Parametertypen">Parametertyp</type>
    </l10n>
  </resource-bundle>

  <properties>
    <property key="projectdoc.doctype.app-parameter.id">
      <resource-bundle>
        <l10n>
          <name>Identifier</name>
          <description>
            Specify the unique parameter identifier.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Identifikator</name>
          <description>
            Spezifizieren Sie den eindeutigen Identifikator des Parameters.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.common.type">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">app-parameter-type</param>
          <param
            name="property"
            key="projectdoc.doctype.common.type" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
    </property>
    <property key="projectdoc.doctype.app-parameter.dataType">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">app-datatype</param>
          <param
              name="property"
              key="projectdoc.doctype.app-parameter.dataType" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
          <param name="property-restrict-one-value">true</param>
        </macro>
      </value>
      <resource-bundle>
        <l10n>
          <name>Data Type</name>
          <description>
            Define the data type for the parameter.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Datentyp</name>
          <description>
            Definieren Sie den Datentyp des Parameters.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.app-parameter.shortType">
      <value>
        <macro name="projectdoc-transclusion-property-display-ref">
          <param name="add-link">true</param>
          <param
              name="property-name"
              key="projectdoc.doctype.app-parameter.dataType" />
          <param
              name="referred-property-name"
              key="projectdoc.doctype.common.shortName" />
        </macro>
      </value>
      <controls>hide</controls>
      <resource-bundle>
        <l10n>
          <name>Short Data Type</name>
        </l10n>
        <l10n locale="de">
          <name>Kurzform des Datentyps</name>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.app-parameter.defaultValue">
      <resource-bundle>
        <l10n>
          <name>Default Value</name>
          <description>
            Specify a default for the parameter.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Default-Wert</name>
          <description>
            Spezifizieren Sie den Default-Wert, falls kein Wert angegeben wird.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.app-parameter.valueRange">
      <resource-bundle>
        <l10n>
          <name>Value Range</name>
          <description>
            List valid values or the range of valid values.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Wertebereich</name>
          <description>
            Spezifizieren den Bereich gültiger Werte.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.app-common.suite">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">app, app-extension, product-space</param>
          <param
              name="property"
              key="projectdoc.doctype.app-common.suite" />
          <param name="defaults-to">ancestor</param>
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
      <controls>hide</controls>
    </property>
    <property key="projectdoc.doctype.app-parameter.since">
      <resource-bundle>
        <l10n>
          <name>Since</name>
          <description>
            Specify since which version this parameter is available.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Seit</name>
          <description>
            Spezifizieren Sie die Version ab der dieser Parameter verfügbar ist.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.app-parameter.deprecated">
      <resource-bundle>
        <l10n>
          <name>Deprecated</name>
          <description>
            Specify since which version this parameter is marked as deprecated.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Veraltet</name>
          <description>
            Spezifizieren Sie die Version ab der dieser Parameter als veraltet
            markiert ist.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.app-parameter.removed">
      <resource-bundle>
        <l10n>
          <name>Removed</name>
          <description>
            Specify since which version this parameter is marked as removed.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Entfernt</name>
          <description>
            Spezifizieren Sie die Version ab der dieser Parameter nicht mehr
            verfügbar ist.
          </description>
        </l10n>
      </resource-bundle>
    </property>
  </properties>

  <sections>
    <section key="projectdoc.doctype.common.description">
      <config>
        <param name="show-title">false</param>
      </config>
      <resource-bundle>
        <l10n>
          <description>
            Describe the use of the parameter.
          </description>
        </l10n>
        <l10n locale="de">
          <description>
            Beschreiben Sie die Bedeutung und die Anwendung des Parameters.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="projectdoc.doctype.app-parameter.details">
      <resource-bundle>
        <l10n>
          <name>Details</name>
          <description>
            Provide further information about the parameter and maybe provide
            usage examples.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Details</name>
          <description>
            Geben Sie weitere, detailliertere Informationen zu diesem Parameter
            an und stellen Sie gegebenenfalls auch Anwendungsbeispiele bereit.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="projectdoc.doctype.app-parameter.usage">
      <xml><![CDATA[<ac:structured-macro ac:name="projectdoc-section">
      <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.app-parameter.usage"/></ac:parameter>
      <ac:parameter ac:name="intro-text"><at:i18n at:key="projectdoc.doctype.app-parameter.usage.intro"/></ac:parameter>
      <ac:rich-text-body>
        <ac:structured-macro ac:name="projectdoc-display-table">
          <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>, <at:i18n at:key="projectdoc.doctype.common.type"/>|, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/></ac:parameter>
          <ac:parameter ac:name="where">$&lt;<at:i18n at:key="projectdoc.doctype.app-common.definedParameters"/>&gt;=[${Name}] OR $&lt;<at:i18n at:key="projectdoc.doctype.app-common.definedPathParameters"/>&gt;=[${Name}] OR $&lt;<at:i18n at:key="projectdoc.doctype.app-common.definedQueryParameters"/>&gt;=[${Name}] OR $&lt;<at:i18n at:key="projectdoc.doctype.app-common.parameters"/>&gt;=[${Name}]</ac:parameter>
          <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
        </ac:structured-macro>
      </ac:rich-text-body>
    </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n>
          <name>Usage</name>
          <description>
            Lists systems that use this parameter for configuration or in their
            interfaces.
          </description>
          <intro>The parameter is used by the following systems.</intro>
        </l10n>
        <l10n locale="de">
          <name>Verwendung</name>
          <description>
            Listet Systeme, die diesen Parameter für ihre Konfiguration oder
            in ihrer Schnittstelle unterstützen.
          </description>
          <intro>
            Der Parameter wird von den folgenden Systemen genutzt.
          </intro>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.app-parameter.relatedParameters">
      <xml><![CDATA[<ac:structured-macro ac:name="projectdoc-section">
      <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.app-parameter.relatedParameters"/></ac:parameter>
      <ac:parameter ac:name="intro-text"><at:i18n at:key="projectdoc.doctype.app-parameter.relatedParameters.intro"/></ac:parameter>
      <ac:rich-text-body>
        <p>
          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">app-parameter</ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="render-mode">definition</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/></ac:parameter>
            <ac:parameter ac:name="where">$&lt;<at:i18n at:key="projectdoc.doctype.common.type"/>&gt;~(${<at:i18n at:key="projectdoc.doctype.common.type"/>})</ac:parameter>
          </ac:structured-macro>
        </p>
      </ac:rich-text-body>
    </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n>
          <name>Related Parameters</name>
          <description>
            Lists parameters with the same type as this parameter.
          </description>
          <intro>List of parameters of the same type.</intro>
        </l10n>
        <l10n locale="de">
          <name>Verwandte Parameter</name>
          <description>
            Listet Parameter mit demselben Parametertyp.
          </description>
          <intro>
            Liste von Parameter mit demselben Parametertyp.
          </intro>
        </l10n>
      </resource-bundle>
    </section>
  </sections>

  <related-doctypes>
    <doctype-ref id="app-code" />
    <doctype-ref id="app-code-type" />
    <doctype-ref id="app-web-resource" />
    <doctype-ref id="app-web-service" />
    <doctype-ref id="app-service" />
    <doctype-ref id="app-component" />
    <doctype-ref id="app-macro" />
    <doctype-ref id="app-datatype" />
  </related-doctypes>
</doctype>
